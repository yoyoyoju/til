# Maven
[maven in 5 minutes](http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html)


* `$ mvn --version` to see the version

* create a project
`$ mvn archetype:generate -DgroupID=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false`

* `pon.xml` is the core of a project's configuration in maven

* to build `mvn package`

# fold
to do: add about the setting

### open and close folds
* `zc`: close the fold
* `zo`: open the fold
* `za`: toggle the fold
* `zC`, `zO`, `zA`: for all levels
* `zr`: open one more level (`zR` for all level)
* `zm`: close one more level (`zM` for all level)
